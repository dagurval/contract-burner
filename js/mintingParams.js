// Configure minting params

// Wallet Connect projectId
const projectId = "f5ea7e2647c4c703993bcdc6b5348a12";

// Url of the API server
const urlApiServer = "https://api.ninjas.cash";

// Contract Params mint
const network = "mainnet";

// Wallet Connect Metadata
const wcMetadata = {
  name: 'Burner-Cash',
  description: 'Burn tokens on BCH',
  url: 'https://burner.cash/',
  icons: ['https://burner.cash/images/logo.png']
};

export { projectId, urlApiServer, network, wcMetadata };