import { Contract, ElectrumNetworkProvider } from "cashscript";
import contractArtifact from "/js/burner.json";
import {  hexToBin, decodeTransaction, cashAddressToLockingBytecode, stringify } from "@bitauth/libauth";
import { ElectrumCluster, ElectrumTransport } from 'electrum-cash';
import { network } from "/js/mintingParams.js";
import { escape as escapeHTML } from 'html-escaper';

document.getElementById('mintSection').style.display = "block";

// Create a custom 1-of-1 electrum cluster for bch-mainnet
const electrumCluster = new ElectrumCluster('Wrap-Cash', '1.4.1', 1, 1);
electrumCluster.addServer('electrum.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
electrumCluster.addServer('cashnode.bch.ninja', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);

const electrum = network == "mainnet" ? electrumCluster : undefined;
// Initialise cashscript ElectrumNetworkProvider
const electrumServer = new ElectrumNetworkProvider(network, electrum);

// Instantiate a new minting contract
const options = { provider: electrumServer };
const contract = new Contract(contractArtifact, [], options);
console.log(`P2sh32 smart contract address is ${contract.tokenAddress}`);
document.getElementById("contractAddress").innerHTML = `<a href="https://explorer.bitcoinunlimited.info/address/${contract.tokenAddress}">${contract.tokenAddress.slice("bitcoincash:".length)}</a>`;

let awaited = false;
const updateUtxos = async () => {
  console.log("update utxos");
  try {
    if (!awaited) {
      await electrumCluster.ready();
      awaited = true;
    }
    const burnableUtxos = await contract.getUtxos();
    if (burnableUtxos instanceof Error) {
      throw burnableUtxos;
    }
    console.log("utxos", burnableUtxos);


    const tokenIDS = new Set(burnableUtxos.map((utxo) => utxo.token?.category));

    const bcmr = new Map(await Promise.all(Array.from(tokenIDS).map(async (category) => {
      try {
        const response = await fetch(`https://indexer.cauldron.quest/bcmr/token/${category}`);
        const bcmrData = await response.json();
        return [category, bcmrData]; // Use array to return a key-value pair
      } catch (e) {
        console.log("bcmr", e);
        return [category, null]; // Use array to ensure the map gets key-value pairs
      }
    })));

    console.log(bcmr);
    console.log(tokenIDS);

    const burnableList = document.getElementById("burnable");

    if (tokenIDS.size === 0) {
      burnableList.innerHTML = '<p style="color: white">No tokens in contract left to burn (refresh after sending tokens)</p>';
    }
    else {

    burnableList.innerHTML = '<div style="color: white;">Burnable tokens:<br /><ul>' + burnableUtxos.map(utxo => {
      if (!utxo.token) return ""; // Skip if no token information

      const meta = bcmr.get(utxo.token.category);
      if (meta) {
        return `<li style="color: white;">${escapeHTML(meta.token.symbol)} &mdash; ${utxo.token.nft ? "NFT" : Number(utxo.token.amount) / Math.pow(10, meta.token.decimals || 0)}</li>`;
      }

      return `<li>${utxo.token.category} &mdash; ${utxo.token.amount}</li>`;
    }).join('') + '</ul></div>';
  }

  } catch (e) {
    console.log(e);
    document.getElementById("error").innerHTML = `Error: ${e}`
  }
}
updateUtxos();

const wrapButton = document.getElementById("wrapButton");
const wrapBCH = async() => {
  try {
    await performWrap();
  }
  catch (e) {
    console.log(e);
    alert(e);
  }
  finally {
    cleanupFailedMint();
  }
}
wrapButton.onclick = wrapBCH;

const cleanupFailedMint = () => {
  wrapButton.textContent = "Burn"
  setTimeout(updateUtxos, 1000);
}

async function performWrap() {
  // Visual feedback for user, disable button onclick
  wrapButton.textContent = `🔥🔥🔥 Burning ... 🔥🔥🔥`;
  wrapButton.onclick = () => { };

  const contractUtxos = await contract.getUtxos();
  console.log("Contract utxos", contractUtxos);

  if (!contractUtxos.length) {
    throw Error("No tokens to burn");
  }

  let transaction = contract.functions.burn().withoutTokenChange().withoutChange().withOpReturn(["🔥🔥🔥"]);
  for (const utxo of contractUtxos) {
    transaction = transaction.from(utxo);
  }

  // try {
    const rawTransactionHex = await transaction.build();
    console.log("Raw tx", rawTransactionHex);

    // hack; not sure why I can't reuse existing connection
     const electrumCluster2 = new ElectrumCluster('Wrap-Cash', '1.4.1', 1, 1);
    electrumCluster2.addServer('electrum.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
    electrumCluster2.addServer('cashnode.bch.ninja', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
    await electrumCluster2.ready();

    const txid = await electrumCluster2.request("blockchain.transaction.broadcast", rawTransactionHex);
    if (txid instanceof Error) {
      throw txid;
    }
    alert(`BURNED IN TX ${txid}`)

  //} catch (error) {
  //  alert(error);
  //  console.log(error);
  //  cleanupFailedMint();
  // }
}
