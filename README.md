# Burner BCH contract and frontend

[https://tokenburner.cash]

Bruner BCH contract provably burns cashtokens by only allowing a single output without tokens in them.

The frontend in this repo creates a single OP RETURN output; giving all sats tied to the token utxo to the miners.

Contract is MIT licensed. Front-end is based on [Cash Ninjas](https://ninjas.cash/) front-end, so their license of picking.

